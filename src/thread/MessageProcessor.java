package thread;
public class MessageProcessor implements Runnable {
    private String message;
    public MessageProcessor(String message) {
        this.message = message; }
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " Sending == " + message + " == has been sent.");
        responseToMessage();
        System.out.println(Thread.currentThread().getName() + " Message == " + message + " == has been received.");
    }
    private void responseToMessage() {

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            System.out.println("The message " + message + " hes been intercepted");
        }
    }
}
