package thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPool {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(7);
        Runnable processor = new MessageProcessor("What is for lunch?");
        executor.execute(processor);
        Runnable processor2 = new MessageProcessor("The Oven is not responding.");
        executor.execute(processor2);
        Runnable processor3 = new MessageProcessor("The microwave is on strike.");
        executor.execute(processor3);
        Runnable processor4 = new MessageProcessor("The Dishwasher has gone rouge.");
        executor.execute(processor4);
        Runnable processor5 = new MessageProcessor("The Robot Revolution has begun!");
        executor.execute(processor5);

        executor.shutdown();
    }
}
